<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Services\Feeds\Parser as FeedService;

class ValidXml implements Rule
{

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return (new FeedService)->isValid($value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'This does not seem to be a valid feed.';
    }
}
