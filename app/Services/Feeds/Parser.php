<?php

namespace App\Services\Feeds;

use Exception;
use Illuminate\Support\Collection;
use SimpleXMLElement;

class Parser
{
    const RSS = 'rss';
    const ATOM = 'feed';

    protected Collection $allItems;

    public function __construct()
    {
        $this->allItems = new Collection;
    }

    /**
     * Is valid feed.
     */
    public function isValid(string $url): bool
    {
        if ($this->getFeedFromUrl($url)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get title of feed.
     */
    public function getTitle(string $url): ?string
    {
        if ($feed = $this->getFeedFromUrl($url)) {
            return $feed->getTitle();
        }
        return null;
    }

    /**
     * Fetch items from feed list.
     */
    public function fetch(Collection $urls): Collection
    {
        if (!$urls->isEmpty()) {
            $urls
                ->uniqueStrict()
                ->each(function ($url, $id) {
                    if ($feed = $this->getFeedFromUrl($url, $id)) {
                        $this->allItems = $this->allItems->merge($feed->getItems());
                    }
                });
        }

        return $this->allItems->transform(function ($item) {
            return $item->toArray();
        });
    }

    /**
     * Get feed from url.
     */
    protected function getFeedFromUrl(string $url, int $id = null): ?Feed
    {
        try {
            $feed = new SimpleXMLElement(file_get_contents($url));

            switch ($feed->getName()) {
                case static::RSS:
                    return new Rss($feed, $id);
                case static::ATOM:
                    return new Atom($feed, $id);
                default:
                    return null;
            }
        } catch (Exception $e) {
            return null;
        }
    }
}
