<?php

namespace App\Services\Feeds;

use Carbon\Carbon;

final class Item
{
    protected int $feedId;
    protected string $headline, $description, $link;
    protected ?string $image;
    protected Carbon $date;

    public function __construct(int $feedId, string $headline, string $description, string $link, string $date = null, string $image = null)
    {
        $this->feedId = $feedId;
        $this->headline = $headline;
        $this->description = $description;
        $this->link = $link;
        $this->image = $image;
        $this->date = new Carbon($date ?? now());
    }

    public function getFeedId(): int
    {
        return $this->feedId;
    }

    public function getHeadline(): string
    {
        return $this->headline;
    }

    public function getDescription(): string
    {
        return strip_tags($this->description);
    }

    public function getLink(): string
    {
        return $this->link;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function getPosted(): string
    {
        return $this->date->diffForHumans();
    }

    public function getTimestamp(): int
    {
        return $this->date->timestamp;
    }

    public function toArray(): array
    {
        return [
            'feed' => $this->getFeedId(),
            'title' => $this->getHeadline(),
            'description' => $this->getDescription(),
            'image' => $this->getImage(),
            'link' => $this->getLink(),
            'posted' => $this->getPosted(),
            'timestamp' => $this->getTimestamp()
        ];
    }
}
