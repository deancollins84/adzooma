<?php

namespace App\Services\Feeds;

use Illuminate\Database\Eloquent\Collection;
use SimpleXMLElement;

abstract class BaseFeed implements Feed
{
    protected ?int $id;
    protected SimpleXMLElement $feed;
    protected Collection $items;

    public function __construct(SimpleXMLElement $feed, int $id = null)
    {
        $this->id = $id;
        $this->feed = $feed;
        $this->items = new Collection();
    }

    abstract public function getItems();

    abstract public function getTitle();
}
