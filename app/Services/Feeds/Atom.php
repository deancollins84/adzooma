<?php

namespace App\Services\Feeds;

class Atom extends BaseFeed implements Feed
{
    public function getItems()
    {
        foreach ($this->feed->entry as $item) {
            $this->items->push(
                new Item(
                    $this->id,
                    $item->title,
                    $item->summary,
                    $item->link->attributes()->href,
                    $item->updated,
                    null
                )
            );
        }

        return $this->items;
    }

    public function getTitle()
    {
        return $this->feed->title;
    }
}
