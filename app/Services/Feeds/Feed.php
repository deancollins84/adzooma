<?php

namespace App\Services\Feeds;

interface Feed
{
    public function getItems();

    public function getTitle();
}
