<?php

namespace App\Services\Feeds;

use SimpleXMLElement;

class Rss extends BaseFeed implements Feed
{
    public function getItems()
    {
        foreach ($this->feed->xpath('//item') as $item) {
            $this->items->push(
                new Item(
                    $this->id,
                    $item->title,
                    $item->description,
                    $item->link,
                    $item->pubDate,
                    $this->getImage($item)
                )
            );
        }

        return $this->items;
    }

    public function getTitle()
    {
        return $this->feed->channel->title;
    }

    protected function getImage(SimpleXMLElement $item): ?string
    {
        if ($media = $item->children('media', true)) {
            return $media->content->attributes()->url;
        }
        return null;
    }
}
