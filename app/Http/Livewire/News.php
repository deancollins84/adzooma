<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Illuminate\Http\Request;
use App\Services\Feeds\Parser as FeedParser;
use Illuminate\Support\Collection;

/**
 * Fetch all news items (feed entries) once, 
 * and then filter if/when requested.
 */
class News extends Component
{

    public ?string $filter;
    public Collection $feedList;
    public Collection $items, $results;

    public function mount(Request $request)
    {
        $this->filter = null;
        $this->feedList = $request->user()->feeds;
        $this->results = new Collection(); // Display results.
        $this->fetchAllItems(); // All news entries.
    }

    /**
     * Display news (feed entries).
     */
    public function render()
    {
        if ($this->filter) {
            $this->results = $this->items->filter(function ($feed) {
                return $feed['feed'] == $this->filter;
            });
        } else {
            $this->results = $this->items;
        }
        return view('livewire.news');
    }

    /** 
     * Fetch news (feed entries) from urls.
     */
    protected function fetchAllItems()
    {
        $this->items =
            (new FeedParser)
            ->fetch(
                $this->feedList->pluck('url', 'id')
            );
    }
}
