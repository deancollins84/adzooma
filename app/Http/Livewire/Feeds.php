<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Feed;
use App\Rules\ValidXml;
use App\Services\Feeds\Parser as FeedParser;
use Symfony\Component\HttpFoundation\Request;
use Illuminate\Session\Store;
use Illuminate\Database\Eloquent\Collection;

class Feeds extends Component
{

    public Collection $feeds;
    public ?string $url;
    public bool $isOpen;

    /**
     * 
     */
    public function mount()
    {
        $this->url = null;
        $this->isOpen = false;
    }

    /**
     * All feeds for user view.
     */
    public function render(Request $request)
    {
        $this->feeds = $request->user()->feeds;
        return view('livewire.feeds');
    }

    /**
     * Prepare form to add feed.
     */
    public function create()
    {
        $this->resetInputFields();
        $this->openModal();
    }

    /**
     * Validate and save feed url.
     */
    public function store(Request $request, FeedParser $feedParser, Store $session)
    {
        $this->validate([
            'url' => ['url', 'required', 'unique:feeds,url', new ValidXml]
        ]); // Http validation requests no supported or recommended with livewire components.

        $request->user()->feeds()->create(
            [
                'url' => $this->url,
                'name' => $feedParser->getTitle($this->url)
            ]
        );

        $session->flash('message', 'Feed added successfully.');

        $this->closeModal();
        $this->resetInputFields();
    }

    public function delete(Feed $feed, Store $session)
    {
        $feed->delete();
        $session->flash('message', 'Feed deleted successfully.');
    }

    /**
     * Open form modal flag.
     */
    public function openModal()
    {
        $this->isOpen = true;
    }

    /**
     * Close form modal flag.
     */
    public function closeModal()
    {
        $this->isOpen = false;
    }

    /**
     * Reset form fields.
     */
    private function resetInputFields()
    {
        $this->url = null;
    }
}
