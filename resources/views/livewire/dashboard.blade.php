<x-slot name="header">
    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
        {{ __('Dashboard') }}
    </h2>
</x-slot>

<div class="py-12">
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        <div class="pb-8">
            <div class="relative">

                <input wire:model="search" type="text" placeholder="Search users..." />
                <select wire:model="filter" class="form-control block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-state">
                    <option value="">Show all</option>
                    @foreach($feedNameList as $id => $name)
                    <option value="{{ $id }}">{{ $name }}</option>
                    @endforeach
                </select>
                <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                    <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                        <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" /></svg>
                </div>
            </div>
        </div>
        <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg px-4 py-4 pt-8">
            @if(!$allFeedItems->isEmpty())
            @foreach($allFeedItems as $feedItem)
            <div class="md:flex pb-8">
                @if($feedItem->getImage())
                <div class="md:flex-shrink-0">
                    <img class="rounded-lg md:w-56" src="https://images.unsplash.com/photo-1556740738-b6a63e27c4df?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=448&q=80" alt="Woman paying for a purchase">
                </div>
                @endif
                <div class="mt-4 md:mt-0 md:ml-6">
                    <div class="uppercase tracking-wide text-sm text-indigo-600 font-bold">{{ $feedItem->getHeadline() }}</div>
                    <p class="mt-2 text-gray-600">{{ $feedItem->getDescription() }}</p>
                    <a href="#" class="block mt-1 text-lg leading-tight font-semibold text-gray-900 hover:underline">More ...</a>
                </div>
            </div>
            @endforeach
            @else
            <p>No feeds have been added.</p>
            @endif
        </div>
    </div>
</div>