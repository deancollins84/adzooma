<div>
    <div class="relative">
        <select wire:model="filter" class="form-control block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-state">
            <option value="">Show all</option>
            @foreach($feedList as $feed)
            <option value="{{ $feed['id'] }}">{{ $feed['name'] }}</option>
            @endforeach
        </select>
        <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
            <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" /></svg>
        </div>
    </div>

    <div class="pt-8">
        @if(!$results->isEmpty())
        @foreach($results->sortByDesc('timestamp') as $item)
        <div class="md:flex p-8">
            @if($item['image'])
            <div class="md:flex-shrink-0">
                <img class="rounded-lg md:w-20" src="{{ $item['image'] }}">
            </div>
            @endif
            <div class="mt-4 md:mt-0 md:ml-6">
                <div class="uppercase tracking-wide text-sm text-indigo-600 font-bold">{{ $item['title'] }}</div>
                <div>{{ $item['posted'] }}</div>
                <p class=" mt-2 text-gray-600">{{ $item['description'] }}</p>
                <a href="{{ $item['link'] }}" class="block mt-1 text-lg leading-tight font-semibold text-gray-900 hover:underline">More ...</a>
            </div>
        </div>
        @endforeach
        @else
        <p>No feeds have been added.</p>
        @endif
    </div>
</div>