# Feed reader

Feed reader to take valid (supported; RSS, ATOM) feed urls and display the list news items in date order.

-   Register and/or sign in.
-   Dashboard display all current feed news items with the ability to filter list by feed.
-   Manage feeds allows new feeds to be added, modified and removed.

# Additional

-   Atom feed support included while developing.

### Tech

-   [Laravel] - Version 8.9.0
-   [Jetstream] - Livewire
-   [Tailwindcss]

### Installation

Clone repo.

```sh
$ git clone git@bitbucket.org:deancollins84/adzooma.git
```

Composer install, npm install and Laravel migrations.

```sh
$ composer install
$ npm install && npm run dev
$ php artisan migrate
$ php artisan key:generate
```

### Examples feeds used

-   http://feeds.bbci.co.uk/news/rss.xml - RSS
-   http://feeds.skynews.com/feeds/rss/uk.xml - RSS
-   https://www.theregister.com/devops/headlines.atom - ATOM
